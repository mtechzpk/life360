package com.example.life360.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.FileUtils
import com.example.life360.R
import com.example.life360.databinding.ActivityHomeBinding
import com.example.life360.utills.Utilities

class HomeActivity : AppCompatActivity() {


    private lateinit var utils: Utilities
    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_home)
        binding = ActivityHomeBinding.inflate(layoutInflater)

        utils = Utilities()
        utils.statusBarTextColorVisibility(window)
    }
}