package com.example.life360.utills

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import java.util.regex.Pattern

class Utilities {


//    val seller = "seller"

    fun openDialog(manager: FragmentManager, fragment: DialogFragment) {
        val ft = manager.beginTransaction()
        val prev = manager.findFragmentByTag("dialog")
        if (prev != null) {
            ft.remove(prev)
        }
        ft.addToBackStack(null)
        fragment.show(ft, "dialog")
    }

    fun getString(context: Context, key: String?): String? {
        val sharedPref = context.getSharedPreferences(
            "SocialCeoSharedStorage",
            Context.MODE_PRIVATE
        )
        return sharedPref.getString(key, "")

/*
        val value =  sharedPref.getString(key, "")
        return value
*/
    }

    fun saveString(context: Context, key: String?, value: String?) {
        val sharedPref =
            context.getSharedPreferences("SocialCeoSharedStorage", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString(key, value)
        editor.apply()
    }

/*
    fun clearSharedPrefStrins(context: Context) {
        context.getSharedPreferences("SocialCeoSharedStorage", Context.MODE_PRIVATE).edit()
            .remove("subjectNameFromGroupsFragment").apply()
    }
*/

    fun isEmailValid(email: String?): Boolean {
        val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }


    //todo:  set statusbar text visibility, iska bg theme.xml me set kiya h
    fun statusBarTextColorVisibility(window: Window) {
//        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
    }

    //todo: transparent toolbar code
    fun transparentToolbar(window: Window, context: Context) {
        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(context as Activity)
            window.statusBarColor = Color.TRANSPARENT
        }
    }

    fun setWindowFlag(activity: Activity) {
        val win = activity.window
        val winParams = win.attributes
        winParams.flags =
            winParams.flags and WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS.inv()
        win.attributes = winParams
    }

    fun startActivity(context: Context) {
//        startActivity(Intent(context, ForgetPassword::class.java))

    }


}